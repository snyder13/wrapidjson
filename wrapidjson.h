#ifndef WRAPIDJSON_H
#define WRAPIDJSON_H

#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

namespace Wrapid {

/**
 * slightly nicer API for rapidjson
 * missing some possible types, though
 */
class ToJson
{
public:
	ToJson() : sb(), writer(sb), objStarted(false) {
	}
	template<typename T>
	auto operator()(const T& val) -> std::string {
		writer.Reset(sb);
		sb.Clear();
		write(val);
		return sb.GetString();
	}
	auto operator()(const char* str) -> std::string {
		return (*this)(std::string(str));
	}
	template<typename T>
	auto operator()(const std::string& key, const T& val) -> ToJson& {
		if (!objStarted) {
			writer.StartObject();
			objStarted = true;
		}
		writer.Key(key.c_str(), key.length());
		write(val);
		return *this;
	}
	auto operator()(const std::string& key, const char* val) -> ToJson& {
		return (*this)(key, std::string(val));
	}
	auto toString() -> std::string {
		if (objStarted) {
			writer.EndObject();
			objStarted = false;
		}
		auto rv = sb.GetString();
		writer.Reset(sb);
		sb.Clear();
		return rv;
	}
private:
	auto write(const std::string& str) -> void {
		writer.String(str.c_str(), str.length());
	}
	auto write(const char* str) -> void {
		writer.String(str);
	}
	auto write(const bool b) -> void {
		writer.Bool(b);
	}
	auto write(const size_t n) -> void {
		writer.Uint64(n);
	}
	auto write(const int64_t n) -> void {
		writer.Int64(n);
	}
	auto write(const int n) -> void {
		writer.Int(n);
	}
	template<typename T>
	auto write(const std::vector<T>& vec) -> void {
		writer.StartArray();
		for (const auto& val: vec) {
			write(val);
		}
		writer.EndArray();
	}
	rapidjson::StringBuffer sb;
	rapidjson::Writer<rapidjson::StringBuffer> writer;
	bool objStarted;
};

class FromJson
{
public:
	FromJson(const std::string& str) : doc() {
		doc.Parse(str.c_str());
	}
	template<typename T>
	auto get(const std::string& key) -> T {
		throw std::runtime_error("no conversion for this type");
	}

private:
	rapidjson::Document doc;
};

template<>
auto FromJson::get(const std::string& key) -> std::string {
	if (!doc.IsObject() || !doc.HasMember(key.c_str())) {
		return "";
	}
	return doc[key.c_str()].GetString();
}

} // ::Wrapid

#endif
